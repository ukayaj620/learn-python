# Introduction

## What is a Program?

Komponen utama dalam sebuah program:
1. Input Output
2. Proses

Untuk menjalankan script atau program python dapat menggunakan command pada terminal yaitu 

python nama_file.py

## Variable 

Tempat untuk menyimpan suatu data dalam memory selama program berjalan.
Jika program sudah selesai dijalankan, atau eksekusi selesai, maka
data yang tersimpan dalam variable akan "dibebaskan" dari memory komputer.

Untuk membuat variable dalam sebuah program harus dilakukan proses deklarasi
variable.

```py
nama_variable = data
```

nama_variable -> identifier dari variable
data -> hal yang ingin disimpan di dalam variable tersebut (string, pecahan, angka bulat,
object).

Pada bahasa python, tipe data dari variable tidak perlu dideklarasikan karena python
akan mengenalinya sendiri.

Proses memasukkan data ke dalam variable disebut **assignment**

## Data Type

Di dalam Python, untuk mengetahui tipe data dari suatu variable dapat menggunakan
code sebagai berikut;

```py
type(nama_variable)
```

Terdapat beberapa jenis tipe data yang umum di dalam Python:
1. **Numbers**
   Dapat dibagi menjadi beberapa tipe data sebagai berikut:
   1. **Integer** (Bilangan Bulat)
      Contoh: ..., -3, -2, -1, 0 , 1, 2, 3, ...

   2. **Float** (Bilangan pecahan)
      Contoh: -8.9, 3.4, 10.4, 2.0
      NB: **.** merupakan koma

     ```py
     # Python Numbers

     # Buat sebuah variable dengan nama angka_bulat dan mengisinya dengan bilangan bulat
     angka_bulat = 90

     # Buat sebuah variable dengan nama angka_pecahan dan mengisinya dengan bilangan pecahan
     angka_pecahan = 14.76
     ```
2. **List** (Kumpulan)
   Tipe data python yang dapat menyimpan sebuah runtunan data.
   Di bahasa pemrogramman lain sering disebut sebagai **Array**.
   
   ```py
   number_1 = 4

   numbers = [4, 56, 90, -10, 24, 55]

   # To access numbers list data
   # syntax: numbers[index_position]

   # Print out number -10
   print(numbers[3])
   ```

   Visualisasi dari bagaimana list menyimpan data

   ![List Representation](images/introduction_data_type_list.JPG)
   
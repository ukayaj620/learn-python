# # Output

# print("OUTPUT EXAMPLE")
# print("Hello, I'm new to Python")

# """

# sprint -> function untuk melakukan output
# print("")
# "" atau '' -> string (teks)

# """

# # Variable

# print("\nVARIABLE EXAMPLE")

# print("Assignment")
# number = 100
# name = "Mr. Python"
# decimals = 10.45

# print(number)
# print(name)
# print(decimals)

# print("\nReassignment")

# number = 78
# name = "Mr. Guido Van Rossum"

# print(number)
# print(name)
# print(decimals)


# Data Type

# Python Numbers

# Buat sebuah variable dengan nama angka_bulat dan mengisinya dengan bilangan bulat
angka_bulat = 90
print(angka_bulat, " has type ", type(angka_bulat))

# Buat sebuah variable dengan nama angka_pecahan dan mengisinya dengan bilangan pecahan
angka_pecahan = 14.76
print(angka_pecahan, " has type ", type(angka_pecahan))

# Python List

numbers = [4, 56, 90, -10, 24, 55]
print(numbers, " has type ", type(numbers))

# Output angka 90 dari list numbers
print(numbers[2])

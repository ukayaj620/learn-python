## Learning Path

1. What is programming and program?
2. Overview about the programming language (Python)
3. Input Output
4. Data Type and Variables
5. Flow Control
   - Selection (If..else, If..elif..else)
   - Iteration (Perulangan)
6. Method (Fuctions)
7. Object Oriented Programming (OOP)
   - Class and Object
   - Method and Attributes
8. Module and Packages
9. Virtual Environment
10. Introduction to API, What is API?
11. Introduction to Binance API (Consuming Binance API)
12. Introduction to Blockchain System (What is blockchain network, testnet, and smart contract)
13. Introduction to Basic Robot Trading (Buy and Sell Coins)
